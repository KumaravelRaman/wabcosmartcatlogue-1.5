package productfamily;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class ProductFamilyItemActivity extends Activity {
    public List<String> Description;
    public List<String> Flag_List;
    public String INPUT_DB_PATH;
    public List<String> Image_List;
    private String Item;
    private String PRODUCT_ID;
    private String ProductName;
    public List<String> Repair_List;
    public List<String> Service_List;
    private FloatingActionButton arrleft;
    private FloatingActionButton arrright;
    private ImageView backImageView,Cart_Icon;
    private Spinner categorySpinner;
    public boolean clicked;
    public LinearLayout flagtext;
    HorizontalScrollView hsv;
    private ProgressDialog loadDialog;
    public List<String> partListList;
    private ListView partListListView;
    private ProductFamilyItemAdapter productFamilyItemAdapter;
    ArrayAdapter<String> productFamilySpinnerAdapter;
    private TextView showAllTextView;
    public List<String> vehicleIDList, productIDList;
    private ArrayAdapter<String> DataAdapter;
    private List<String> vehicleNameList;
    public List<String> vehicleNameList2;
    public List<Bitmap> productbitmap;
    private Bitmap bitmap;
    Alertbox box = new Alertbox(ProductFamilyItemActivity.this);
    private ImageView cart_icon;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productfamily_partlist);
        hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        backImageView = (ImageView) findViewById(R.id.back);
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();


        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        vehicleNameList = new ArrayList<String>();
        productIDList = new ArrayList<String>();
        Item = "not";
        clicked = false;
        cart_icon = (ImageView) findViewById(R.id.cart_icon);

        backImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });


        arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });

        Intent partItem = getIntent();
        ProductName = partItem.getStringExtra("PRODUCTFAMILY_NAME");
        PRODUCT_ID = partItem.getStringExtra("PRODUCT_ID");

        TextView title = (TextView) findViewById(R.id.tittle);
        flagtext = (LinearLayout) findViewById(R.id.flagtxt);
        title.setText(ProductName);
        getSharedPreferences("DOWNLOAD_CHECK", 0);

        new RetrieveSpinnerData().execute();

        showAllTextView = (TextView) findViewById(R.id.partlist_showall);
        categorySpinner = (Spinner) findViewById(R.id.partlist_spinner);

        categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Item = Integer.toString(i);
                Log.v("u Clicked in spinner", Integer.toString(i));
                if (!Item.equals("0")) {
                    Log.v("this is if", Item);
                    new RetrieveFileAsynforselect().execute();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        partListListView = (ListView) findViewById(R.id.partlist_listView);

        showAllTextView.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                new RetrieveFileAsyn().execute();
            }
        });

        cart_icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //startActivity(new Intent(ProductFamilyItemActivity.this, Cart_Activity.class));
            }
        });


        new RetrieveFileAsyn().execute();


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(ProductFamilyItemActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(ProductFamilyItemActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(ProductFamilyItemActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(ProductFamilyItemActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(ProductFamilyItemActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(ProductFamilyItemActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(ProductFamilyItemActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(ProductFamilyItemActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(ProductFamilyItemActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(ProductFamilyItemActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(ProductFamilyItemActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetrieveFileAsyn extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(ProductFamilyItemActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();

            vehicleNameList2 = new ArrayList<String>();
            partListList = new ArrayList<String>();
            vehicleIDList = new ArrayList<String>();
            partListList = new ArrayList<String>();
            productbitmap = new ArrayList<Bitmap>();
            Image_List = new ArrayList<String>();
        }

        protected String doInBackground(Void... arg0) {
            ProductFamilyDAO productFamilyDAO = new ProductFamilyDAO(ProductFamilyItemActivity.this);
            ProductFamilyDTO productFamilyDTO = new ProductFamilyDTO();
            productFamilyDTO = productFamilyDAO.retrieveALLForAll(PRODUCT_ID);
            vehicleNameList2 = productFamilyDTO.getVehicleName();
            Description = productFamilyDTO.getDescription();
            partListList = productFamilyDTO.getPartno_List();
            Image_List = productFamilyDTO.getService_List();
            Flag_List = productFamilyDTO.getFlag_List();
            vehicleIDList = productFamilyDTO.getVehicleIDList();
            productIDList = productFamilyDTO.getProductIDList();
            if (vehicleNameList2.isEmpty()) {
                return "Empty";
            }
            Log.v("Image names", Image_List.toString());
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                flagtext.setVisibility(View.INVISIBLE);
                box.showAlertbox("No data found !");

            } else {

                DataAdapter.notifyDataSetChanged();
                productFamilyItemAdapter = new ProductFamilyItemAdapter(ProductFamilyItemActivity.this, vehicleIDList, vehicleNameList2, Description, partListList, ProductName, Flag_List, Image_List, productIDList);
                partListListView.setAdapter(productFamilyItemAdapter);
                arrright.setVisibility(View.VISIBLE);

                if (!Flag_List.isEmpty()) {
                    flagtext.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    class RetrieveFileAsynforselect extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(ProductFamilyItemActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();


            vehicleNameList2 = new ArrayList<String>();
            partListList = new ArrayList<String>();
            vehicleIDList = new ArrayList<String>();
            partListList = new ArrayList<String>();
            productbitmap = new ArrayList<Bitmap>();
            Image_List = new ArrayList<String>();
        }

        protected String doInBackground(Void... arg0) {
            ProductFamilyDAO productFamilyDAO = new ProductFamilyDAO(ProductFamilyItemActivity.this);
            ProductFamilyDTO productFamilyDTO = new ProductFamilyDTO();
            productFamilyDTO = productFamilyDAO.retriveForVehicle(Item, PRODUCT_ID);

            vehicleNameList2 = productFamilyDTO.getVehicleName();
            Description = productFamilyDTO.getDescription();
            partListList = productFamilyDTO.getPartno_List();
            Image_List = productFamilyDTO.getService_List();
            vehicleIDList = productFamilyDTO.getVehicleIDList();
            Flag_List = productFamilyDTO.getFlag_List();
            productIDList.add(PRODUCT_ID);
            if (vehicleNameList2.isEmpty()) {
                return "Empty";
            }

            Log.v("Image names", Image_List.toString());
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                flagtext.setVisibility(View.GONE);
                box.showAlertbox("No UserData Found !");

                productFamilyItemAdapter.clear();
                partListListView.setAdapter(productFamilyItemAdapter);
                return;
            }


            if (!Flag_List.isEmpty()) {
                flagtext.setVisibility(View.VISIBLE);
            }

            DataAdapter.notifyDataSetChanged();
            productFamilyItemAdapter = new ProductFamilyItemAdapter(ProductFamilyItemActivity.this, vehicleIDList, vehicleNameList2, Description, partListList, ProductName, Flag_List, Image_List, productIDList);
            arrright.setVisibility(View.VISIBLE);
            partListListView.setAdapter(productFamilyItemAdapter);

        }
    }

    class RetrieveSpinnerData extends AsyncTask<Void, Void, String> {


        protected String doInBackground(Void... params) {
            ProductFamilyDAO productFamilyDAO = new ProductFamilyDAO(ProductFamilyItemActivity.this);
            new ProductFamilyDTO();
            vehicleNameList = productFamilyDAO.retrieveVehicleName().getVehicleNameListForSpinner();
            vehicleNameList.add(0, "Select Vehicle Make");
            return null;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            DataAdapter = new ArrayAdapter<String>(ProductFamilyItemActivity.this, R.layout.simple_spinner_item, vehicleNameList);
            //DataAdapter.setDropDownViewResource(17367049);
            DataAdapter.notifyDataSetChanged();
            categorySpinner.setSelection(0);
            categorySpinner.setAdapter(DataAdapter);
        }
    }


    private Bitmap downloadImage(String url) {

        try {
            InputStream fileInputStream = getAssets().open(url);
            Options bmOptions = new Options();
            bmOptions.inSampleSize = 5;
            bitmap = BitmapFactory.decodeStream(fileInputStream, null, bmOptions);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Error", e.getMessage());
            return downloadImage("noimagefound");

        }
        return bitmap;
    }

    public static int calculateInSampleSize(Options options, int imageWidth, int imageHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        if (height <= imageWidth && width <= imageWidth) {
            return 1;
        }
        int heightRatio = Math.round(((float) height) / ((float) imageHeight));
        int widthRatio = Math.round(((float) width) / ((float) imageWidth));
        if (heightRatio < widthRatio) {
            return heightRatio;
        }
        return widthRatio;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
