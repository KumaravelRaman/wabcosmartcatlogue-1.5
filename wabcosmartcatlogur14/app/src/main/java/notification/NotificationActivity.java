package notification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import address.distributoraddress.Select_DistributorActivity;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import api.ErrorUtils;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.distributor.DistributorModel;
import models.errormodel.APIError;
import models.whatsnew.WhatsData;
import models.whatsnew.WhatsNewView;
import network.NetworkConnection;
import okhttp3.ResponseBody;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import registration.ServerConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class NotificationActivity extends Activity {
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ArrayList<String> attachname;
    private ImageView backImageView;
    public Connection connection;
    private ArrayList<String> date;
    private ArrayList<String> discription;
    private ArrayList<String> name;
    public ProgressDialog progressDialog;
    RecyclerView recyclerView;
    public ResultSet rset;
    private ProgressDialog loading;
    public Statement stmt;
    private Alertbox box = new Alertbox(NotificationActivity.this);
    private List<WhatsData> whatsnewdata;

    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        this.recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.attachname = new ArrayList<String>();
        this.name = new ArrayList<String>();
        this.discription = new ArrayList<String>();
        this.date = new ArrayList<String>();
        this.backImageView = (ImageView) findViewById(R.id.back);
        
        this.backImageView.setOnClickListener(new OnClickListener() {
			
        	public void onClick(View arg0)
        	{
                onBackPressed();
            }
		});
        
        final ImageView menu = (ImageView)findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(NotificationActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					
                	  public boolean onMenuItemClick(MenuItem item) {
                          switch (item.getItemId()) {
                              case R.id.home:
                                  startActivity(new Intent(NotificationActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                  break;

                              case R.id.search /*2131427348*/:
                                  startActivity(new Intent(NotificationActivity.this, SearchActivity.class));
                                  break;
                              case R.id.notification /*2131427389*/:
                                  startActivity(new Intent(NotificationActivity.this, NotificationActivity.class));
                                  break;
                              case R.id.vehicle /*2131427459*/:
                                  startActivity(new Intent(NotificationActivity.this, VehicleMakeActivity.class));
                                  break;
                              case R.id.product /*2131427460*/:
                                  startActivity(new Intent(NotificationActivity.this, ProductFamilyActivity.class));
                                  break;
                              case R.id.performance /*2131427461*/:
                                  startActivity(new Intent(NotificationActivity.this, PE_Kit_Activity.class));
                                  break;
                              case R.id.contact:
                                  startActivity(new Intent(NotificationActivity.this, Network_Activity.class));
                                  break;
                              case R.id.askwabco:
                                  startActivity(new Intent(NotificationActivity.this, AskWabcoActivity.class));
                                  break;
                              case R.id.pricelist:
                                  startActivity(new Intent(NotificationActivity.this, PriceListActivity.class));
                                  break;
                              case R.id.update:
      							WabcoUpdate update = new WabcoUpdate(NotificationActivity.this);
      							update.checkVersion();
      							break;
                          }
                          return false;
                      }
				});
				pop.setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(PopupMenu arg0) {
						// TODO Auto-generated method stub
						pop.dismiss();
					}
				});

				pop.inflate(R.menu.main);
				pop.show();
			}
		});
        checkInternet();
    }

    private void Getwhatsnewdata() {
        loading = ProgressDialog.show(NotificationActivity.this, "Loading", "Please wait...",
                false, false);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);

        Call<WhatsNewView> distributor = api.whatsnew();
        distributor.enqueue(new Callback<WhatsNewView>() {
            @Override
            public void onResponse(Call<WhatsNewView> distributorList,
                                   Response<WhatsNewView> response) {
                loading.dismiss();
                if (response.isSuccessful()) {
                    whatsnewdata=response.body().getData();
                    recyclerView.setAdapter(new NotifiAdapter(NotificationActivity.this, whatsnewdata));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    ResponseBody errorBody = response.errorBody();
                    APIError apiError = ErrorUtils.parseError(response);

                    StyleableToast st = new StyleableToast(NotificationActivity.this,
                            apiError.message(), Toast.LENGTH_SHORT);
                    st.setBackgroundColor(
                            NotificationActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }

            @Override
            public void onFailure(Call<WhatsNewView> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));
            }
        });
    }

    
//    public class NotifiDetails extends AsyncTask<String, Void, String> {
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(NotificationActivity.this);
//            progressDialog.setProgressStyle(0);
//            progressDialog.setCancelable(true);
//            progressDialog.setMessage("Loading data...");
//            if(!progressDialog.isShowing())
//            progressDialog.show();
//            name.clear();
//            attachname.clear();
//            discription.clear();
//            date.clear();
//        }
//
//        protected String doInBackground(String... params) {
//            int i = 0;
//            try {
//                ServerConnection ser = new ServerConnection();
//                connection = ser.getConnection();
//                stmt = connection.createStatement();
//                rset = stmt.executeQuery("select id,Notificationname,Discription from Notification where status= 'Active' and deletestatus='notdelete' order by id desc");
//                while (rset.next())
//                {
//                    name.add(rset.getString("Notificationname"));
//                    discription.add(rset.getString("Discription"));
//                    date.add(rset.getString("Discription"));
//
//                    Log.v("des", (String) discription.get(i));
//
//                    i++;
//                }
//                if (name.isEmpty()) {
//                    return "empty";
//                }
//                rset.close();
//                stmt.close();
//                connection.close();
//                return "success";
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "unsuccess";
//            }
//        }
//
//        @SuppressWarnings("deprecation")
//		protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            if (result.equals("success"))
//            {
//                recyclerView.setAdapter(new NotifiAdapter(NotificationActivity.this, name, discription));
//                recyclerView.setItemAnimator(new DefaultItemAnimator());
//            }
//            else if(result.equals("empty"))
//            {
//                showAlert("No notification found !");
//            }else if(result.equals("unsuccess")){
//                showAlert("please try again !");
//            }
//            else {
//            	showAlert(getResources().getString(R.string.nointernetmsg));
//			}
//        }
//    }

    private void checkInternet() {
        if (new NetworkConnection(this).CheckInternet()) {
            Getwhatsnewdata();
//            new NotifiDetails().execute(new String[0]);
        } else {
            showAlert(getResources().getString(R.string.nointernetmsg));
        }
    }

    public void showAlert(String msg) {
    	Alertbox box = new Alertbox(NotificationActivity.this);
		box.showAlertbox(msg);
    }
}
