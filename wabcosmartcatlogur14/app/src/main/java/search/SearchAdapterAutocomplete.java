package search;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;
import java.util.List;

public class SearchAdapterAutocomplete extends ArrayAdapter<String> implements Filterable {
    private List<String> fullList;
    private ArrayFilter mFilter;
    private ArrayList<String> mOriginalValues;

    private class ArrayFilter extends Filter {
        private Object lock;

        private ArrayFilter() {
        }

        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();
            if (SearchAdapterAutocomplete.this.mOriginalValues == null) {
                synchronized (this.lock) {
                    SearchAdapterAutocomplete.this.mOriginalValues = new ArrayList<String>(SearchAdapterAutocomplete.this.fullList);
                }
            }
            if (prefix == null || prefix.length() == 0) {
                synchronized (this.lock) {
                    ArrayList<String> list = new ArrayList<String>(SearchAdapterAutocomplete.this.mOriginalValues);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                String prefixString = prefix.toString().toLowerCase();
                ArrayList<String> values = SearchAdapterAutocomplete.this.mOriginalValues;
                int count = values.size();
                ArrayList<String> newValues = new ArrayList<String>(count);
                for (int i = 0; i < count; i++) {
                    String item = (String) values.get(i);
                    if (item.toLowerCase().contains(prefixString)) {
                        newValues.add(item);
                    }
                }
                results.values = newValues;
                results.count = newValues.size();
            }
            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                SearchAdapterAutocomplete.this.fullList = (ArrayList<String>) results.values;
            } else {
                SearchAdapterAutocomplete.this.fullList = new ArrayList<String>();
            }
            if (results.count > 0) {
                SearchAdapterAutocomplete.this.notifyDataSetChanged();
            } else {
                SearchAdapterAutocomplete.this.notifyDataSetInvalidated();
            }
        }
    }

    public SearchAdapterAutocomplete(Context context, int resource, int textViewResourceId, List<String> fullList) {
        super(context, resource, textViewResourceId, fullList);
        this.fullList = fullList;
        this.mOriginalValues = new ArrayList<String>(fullList);
    }

    public int getCount() {
        return this.fullList.size();
    }

    public String getItem(int position) {
        return (String) this.fullList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    public Filter getFilter() {
        if (this.mFilter == null) {
            this.mFilter = new ArrayFilter();
        }
        return this.mFilter;
    }
}
