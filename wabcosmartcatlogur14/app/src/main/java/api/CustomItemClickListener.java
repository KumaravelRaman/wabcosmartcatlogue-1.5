package api;

import android.view.View;

/**
 * Created by system01 on 6/24/2017.
 */

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
