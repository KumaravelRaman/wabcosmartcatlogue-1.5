package adapter;

import static android.content.Context.MODE_PRIVATE;
import static com.wabco.brainmagic.wabco.catalogue.R.id.sno;

import java.util.List;

import com.dd.CircularProgressButton;
import com.wabco.brainmagic.wabco.catalogue.R;

import alertbox.Alertbox;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import dealer.completed.Dealer_Completed_Order_Details_Activity;
import models.dealer.pending.request.query.QueryOrder;
import models.dealer.pending.request.query.QueryPart;
import models.dealer.pending.responce.OrderDetailsResult;
import serviceengineer.completed.FieldEngineer_Completed_Orders_Details_Activity;


public class Completed_Order_Adapter extends ArrayAdapter<OrderDetailsResult> {
  private final SharedPreferences dealershare;
  private final SharedPreferences.Editor dealeredit;
  private Context context;
  List<OrderDetailsResult> orderDetailsResults;
  private QueryPart query;
  private Alertbox box;
  private QueryOrder queryorder;


  public Completed_Order_Adapter(Context context, List<OrderDetailsResult> orderDetailsResults) {
    super(context, R.layout.adapter_complete_order, orderDetailsResults);
    this.orderDetailsResults = orderDetailsResults;
    this.context = context;
    dealershare = context.getSharedPreferences("registration", MODE_PRIVATE);
    dealeredit = dealershare.edit();
    box = new Alertbox(context);
  }

  public View getView(final int position, View convertView, ViewGroup parent) {
    final PendingOrder pendingOrderHolder;
    if (convertView == null) {

      convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
          .inflate(R.layout.adapter_complete_order, null);
      pendingOrderHolder = new PendingOrder();
      pendingOrderHolder.sno = (TextView) convertView.findViewById(sno);
      pendingOrderHolder.orderNumber = (TextView) convertView.findViewById(R.id.ordernumber);
      pendingOrderHolder.orderDate = (TextView) convertView.findViewById(R.id.orderdate);
      pendingOrderHolder.orderMrp = (TextView) convertView.findViewById(R.id.ordermrp);
      pendingOrderHolder.orderStatus = (TextView) convertView.findViewById(R.id.orderstatus);
      pendingOrderHolder.distributoName = (TextView) convertView.findViewById(R.id.distributorname);
      pendingOrderHolder.viewmore = (Button) convertView.findViewById(R.id.viewmore);
      convertView.setTag(pendingOrderHolder);

    } else {
      pendingOrderHolder = (PendingOrder) convertView.getTag();
    }
    pendingOrderHolder.sno.setText(Integer.toString(position + 1) + ".");


    String[] splitdate = orderDetailsResults.get(position).getOrderedDate().split("T");
    pendingOrderHolder.orderDate.setText(splitdate[0]);

    pendingOrderHolder.orderNumber.setText(orderDetailsResults.get(position).getOrderNumber());
    pendingOrderHolder.orderNumber.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

    pendingOrderHolder.orderMrp.setText(context.getString(R.string.Rs) + " "
        + orderDetailsResults.get(position).getTotalOrderAmount().toString() + "0");

    pendingOrderHolder.orderStatus.setText(orderDetailsResults.get(position).getOrderStatus());
    pendingOrderHolder.distributoName.setText(orderDetailsResults.get(position).getDistrName());

    pendingOrderHolder.orderNumber.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        // TODO Auto-generated method stub

        if (dealershare.getString("usertype", "Wabco employee").equals("Wabco employee")) {
          Intent gotoOrderDetails = new Intent(context, FieldEngineer_Completed_Orders_Details_Activity.class)
                  .putExtra("orderObject", orderDetailsResults.get(position));
          context.startActivity(gotoOrderDetails);
        } else {
          Intent gotoOrderDetails = new Intent(context, Dealer_Completed_Order_Details_Activity.class)
                  .putExtra("orderObject", orderDetailsResults.get(position));
          context.startActivity(gotoOrderDetails);
        }
      }

    });


    pendingOrderHolder.viewmore.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (dealershare.getString("usertype", "Wabco employee").equals("Wabco employee")) {
          Intent gotoOrderDetails = new Intent(context, FieldEngineer_Completed_Orders_Details_Activity.class)
                //  .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                  .putExtra("orderObject", orderDetailsResults.get(position));
          context.startActivity(gotoOrderDetails);
        } else {
          Intent gotoOrderDetails = new Intent(context, Dealer_Completed_Order_Details_Activity.class)
                 // .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                  .putExtra("orderObject", orderDetailsResults.get(position));
          context.startActivity(gotoOrderDetails);
        }
      }
    });


    return convertView;

  }

  class PendingOrder {
    TextView orderNumber, orderDate, orderMrp, distributoName, orderStatus, sno, help;
    CircularProgressButton cancelbtn;
    Button viewmore;
  }


}
