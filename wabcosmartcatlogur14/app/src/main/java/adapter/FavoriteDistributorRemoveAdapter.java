package adapter;

import static android.content.Context.MODE_PRIVATE;

import java.util.List;

import com.wabco.brainmagic.wabco.catalogue.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import models.distributor.DistributorData;


public class FavoriteDistributorRemoveAdapter extends ArrayAdapter<DistributorData> {
  private Context context;
  private List<DistributorData> distributorData;
  private int selectedPosition = -1;
  private SharedPreferences distributor_share;
  private SharedPreferences.Editor distributor_edit;

  public FavoriteDistributorRemoveAdapter(Context context, List<DistributorData> data) {
    super(context, R.layout.adapter_favorite_distributor,data);
    this.context = context;
    this.distributorData = data;
    distributor_share = context.getSharedPreferences("distributor_address", MODE_PRIVATE);
    distributor_edit = distributor_share.edit();

  }

  public View getView(final int position, View convertView, ViewGroup parent) {
    final DistributorHolder contactHolder;
    if (convertView == null) {
      convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
          .inflate(R.layout.adapter_favorite_distributor, null);
      contactHolder = new DistributorHolder();
      contactHolder.name = (TextView) convertView.findViewById(R.id.dist_name);
      contactHolder.address = (TextView) convertView.findViewById(R.id.dist_address);
      contactHolder.selectdistributor= (CheckBox) convertView.findViewById(R.id.select_distributor);
      convertView.setTag(contactHolder);
    } else {
      contactHolder = (DistributorHolder) convertView.getTag();
    }
    contactHolder.address.setText((CharSequence) distributorData.get(position).getAddress());
    contactHolder.name.setText((CharSequence) distributorData.get(position).getName());

    //in some cases, it will prevent unwanted situations
    contactHolder.selectdistributor.setOnCheckedChangeListener(null);
    //if true, your checkbox will be selected, else unselected
    contactHolder.selectdistributor.setChecked(distributorData.get(position).getFavorite());
    contactHolder.selectdistributor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        contactHolder.selectdistributor.setChecked(isChecked);
        distributorData.get(position).setFavorite(isChecked);
      }
    });

    return convertView;

  }



  @Override
  public int getCount() {
    return distributorData.size();
  }

  class DistributorHolder {
    TextView name, address;
    CheckBox selectdistributor;
  }


}
