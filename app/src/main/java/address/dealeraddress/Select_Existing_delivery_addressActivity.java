package address.dealeraddress;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wabco.brainmagic.wabco.catalogue.R;

import addcart.Order_ConfirmationActivity;

public class Select_Existing_delivery_addressActivity extends Activity {

    private Button Delivery,Skip,NewDelivery,EditBtn,DeleteBtn;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_delivery_address);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        Delivery = (Button) findViewById(R.id.delivery_address);
        Skip = (Button) findViewById(R.id.skip);

        NewDelivery = (Button) findViewById(R.id.new_address);
        EditBtn = (Button) findViewById(R.id.skip);
        DeleteBtn = (Button) findViewById(R.id.skip);



        Delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Select_Existing_delivery_addressActivity.this, Order_ConfirmationActivity.class));

                /*if(myshare.getBoolean("islogin",false))
                {
                    startActivity(new Intent(Select_Existing_delivery_addressActivity.this, Order_ConfirmationActivity.class));
                }
                else
                {
                    startActivity(new Intent(Select_Existing_delivery_addressActivity.this, UserLoginActivity.class));
                }
*/

            }
        });


        Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Select_Existing_delivery_addressActivity.this, Order_ConfirmationActivity.class));
            }
        });

        NewDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Select_Existing_delivery_addressActivity.this, Add_delivery_address_Activity.class));
            }
        });

    }

}
