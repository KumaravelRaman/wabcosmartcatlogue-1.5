package directory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import network.NetworkConnection;


public class Updation {
    private static final String FILENAME = "wabco.zip";
    private Activity context;
    private DownloadDatabaseFile downloadDatabaseFile;
    private ProgressDialog downloadDialog;
    private Editor editor;
    private SharedPreferences preferences;
    private ProgressDialog updateCheckDialog;

    class CheckUpdateAsyn extends AsyncTask<Void, Void, String> {
       

        protected void onPreExecute() {
            super.onPreExecute();
            Updation.this.updateCheckDialog = new ProgressDialog(Updation.this.context);
            Updation.this.updateCheckDialog.setCancelable(false);
            Updation.this.updateCheckDialog.setTitle("Checking update...");
            Updation.this.updateCheckDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            try {
                return Updation.this.checkFile();
            } catch (Exception e) {
                return "error_success";
            }
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Updation.this.updateCheckDialog.dismiss();
            String checkFileStorageArea = Updation.this.preferences.getString("DOWNLOAD_FILE", null);
            if (result.equals("download")) {
                if (checkFileStorageArea.equals("INTERNAL")) {
                    Updation.this.downloadUpdates();
                } else if (checkFileStorageArea.equals("EXTERNAL")) {
                    Updation.this.downloadUpdates();
                }
            } else if (result.equals("no_download")) {
                Toast.makeText(Updation.this.context, "File up to date", 0).show();
            } else if (result.equals("error_success")) {
                new NetworkConnection(Updation.this.context).alertDialogShow();
            }
        }
    }

    class DownloadAsynCheck extends AsyncTask<Void, Void, String> {
       
        protected void onPreExecute() {
            super.onPreExecute();
            Updation.this.downloadDialog = new ProgressDialog(Updation.this.context);
            Updation.this.downloadDialog.setCancelable(false);
            Updation.this.downloadDialog.setTitle("Loading...");
            Updation.this.downloadDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            try {
                return Updation.this.downloadFile();
            } catch (Exception e) {
                return "error_success";
            }
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Updation.this.downloadDialog.dismiss();
            String checkFileStorageArea = Updation.this.preferences.getString("DOWNLOAD_FILE", null);
            File filePath;
            if (checkFileStorageArea.equals("INTERNAL")) {
                filePath = new File(context.getDir("wabcodb", 0), Updation.FILENAME);
                downloadDatabaseFile.showProgress();
                //downloadDatabaseFile.downloadOnlineFile(filePath);
            } else if (checkFileStorageArea.equals("EXTERNAL")) {
                File directory = new File(Environment.getExternalStorageDirectory(), "wabcodb");
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                filePath = new File(directory, Updation.FILENAME);
                Updation.this.downloadDatabaseFile.showProgress();
             //   Updation.this.downloadDatabaseFile.downloadOnlineFile(filePath);
            } else if (result.equals("error_success")) {
                new NetworkConnection(Updation.this.context).alertDialogShow();
            }
        }
    }

    public Updation(Activity context) {
        this.context = context;
        this.preferences = context.getSharedPreferences("DOWNLOAD_CHECK", 0);
        this.editor = this.preferences.edit();
        this.preferences = context.getSharedPreferences("DOWNLOAD_CHECK", 0);
        this.editor = this.preferences.edit();
        if (Boolean.valueOf(this.preferences.getBoolean("DOWNLOAD_DATA", true)).booleanValue()) {
            checkConnection();
        }
    }

    public void checkConnection() {
        NetworkConnection connection = new NetworkConnection(this.context);
        if (connection.CheckInternet()) {
            Log.e("ASYN", "START");
            new DownloadAsynCheck().execute(new Void[0]);
            return;
        }
        connection.alertDialogShow();
    }

    public String downloadFile() {
        CheckExternalMemory checkExternalMemory = new CheckExternalMemory();
        CheckInternalMemory checkInternalMemory = new CheckInternalMemory();
        CheckOnlineFileSize checkOnlineFileSize = new CheckOnlineFileSize("");
        this.downloadDatabaseFile = new DownloadDatabaseFile(this.context);
        double externalFileSize = checkExternalMemory.getExternalMemorySize();
        double internalFileSize = checkInternalMemory.getInternalMemorySize();
        double downloadFileSize = checkOnlineFileSize.checkFileSize();
        if (internalFileSize > downloadFileSize) {
            Log.e("INTERNAL", "START");
            this.editor.putString("DOWNLOAD_FILE", "INTERNAL");
            this.editor.commit();
            return "INTERNAL";
        } else if (externalFileSize <= downloadFileSize) {
            return null;
        } else {
            Log.e("EXTERNAL", "START");
            this.editor.putString("DOWNLOAD_FILE", "EXTERNAL");
            this.editor.commit();
            return "EXTERNAL";
        }
    }

    public void checkUpdateConnection() {
        NetworkConnection connection = new NetworkConnection(this.context);
        if (connection.CheckInternet()) {
            Log.e("ASYN", "START");
            new CheckUpdateAsyn().execute(new Void[0]);
            return;
        }
        connection.alertDialogShow();
    }

    private String checkFile() {
        String checkUpdates = "no_download";
        CheckOnlineFileModifiedDate checkOnlineFileModifiedDate = new CheckOnlineFileModifiedDate();
        String fileModifiedDate = this.preferences.getString("FILE_DATE", null);
        String checkOnlineDate = checkOnlineFileModifiedDate.checkFileDate();
        String checkFileStorageArea = this.preferences.getString("DOWNLOAD_FILE", null);
        if (checkFileStorageArea.equals("INTERNAL")) {
            if (!new File("/data/data/" + this.context.getPackageName() + "/" + "app_wabcodb/wabco").exists()) {
                return "download";
            }
            if (checkOnlineDate.equals(fileModifiedDate)) {
                return "no_download";
            }
            return "download";
        } else if (!checkFileStorageArea.equals("EXTERNAL")) {
            return checkUpdates;
        } else {
            if (!new File(Environment.getExternalStorageDirectory() + "/wabcodb/" + FILENAME.toString()).exists()) {
                return "download";
            }
            if (checkOnlineDate.equals(fileModifiedDate)) {
                return "no_download";
            }
            return "download";
        }
    }

    private void downloadUpdates() {
        CheckExternalMemory checkExternalMemory = new CheckExternalMemory();
        CheckInternalMemory checkInternalMemory = new CheckInternalMemory();
        CheckOnlineFileSize checkOnlineFileSize = new CheckOnlineFileSize("");
        DownloadDatabaseFile downloadDatabaseFile = new DownloadDatabaseFile(this.context);
        double externalFileSize = checkExternalMemory.getExternalMemorySize();
        double internalFileSize = checkInternalMemory.getInternalMemorySize();
        double downloadFileSize = checkOnlineFileSize.checkFileSize();
        File filePath;
        if (internalFileSize > downloadFileSize) {
            Log.e("INTERNAL", "START");
            this.editor.putString("DOWNLOAD_FILE", "INTERNAL");
            this.editor.commit();
            filePath = new File(this.context.getDir("wabcodb", 0), FILENAME);
            downloadDatabaseFile.showProgress();
          //  downloadDatabaseFile.downloadOnlineFile(filePath);
        } else if (externalFileSize > downloadFileSize) {
            Log.e("EXTERNAL", "START");
            this.editor.putString("DOWNLOAD_FILE", "EXTERNAL");
            this.editor.commit();
            File directory = new File(Environment.getExternalStorageDirectory(), "wabcodb");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            filePath = new File(directory, FILENAME);
            downloadDatabaseFile.showProgress();
           // downloadDatabaseFile.downloadOnlineFile(filePath);
        }
    }

    public static double getFolderSize(File f) {
        double size = 0.0d;
        if (!f.isDirectory()) {
            return (double) f.length();
        }
        for (File file : f.listFiles()) {
            size += getFolderSize(file);
        }
        return size;
    }
}
/* private void requestStoragePermission()
    {
 
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE))
        {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        	showAlertBox("Wabco Application needs this Permission for updation");
        }
 
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},STORAGE_PERMISSION_CODE);
    }
	
	
	private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
 
        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
 
        //If permission is not granted returning false
        return false;
    }
	
	
	
	@SuppressLint("NewApi") @Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if(requestCode== REQUEST_WRITE_STORAGE)
		{
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
			{
				//reload my activity with permission granted or use the features what required the permission
				try {
					Log.v("Version code ", Integer.toString(version));
					//Log.v("file==== Write mail ", root);

					File myFile = new File(root);
					myFile.createNewFile();
					FileOutputStream fOut = new FileOutputStream(myFile);
					OutputStreamWriter myOutWriter = 
							new OutputStreamWriter(fOut);
					myOutWriter.append(Integer.toString(version));
					myOutWriter.close();
					fOut.close();

					
				} 
				catch (Exception e) 
				{
					Toast.makeText(getBaseContext(), e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}

			} else
			{
				Toast.makeText(MainActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
			}
		}
		else if(requestCode== STORAGE_PERMISSION_CODE) 
		{
			//If permission is granted
			if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

				
				try {
					File myFile = new File(root);
					FileInputStream fIn = new FileInputStream(myFile);
					BufferedReader myReader = new BufferedReader(
							new InputStreamReader(fIn));
					String aDataRow = "";
					String aBuffer = "";
					while ((aDataRow = myReader.readLine()) != null)
					{
						aBuffer += aDataRow + "\n";
					}
					//txtData.setText(aBuffer);
					editor.putInt("VERSION", Integer.parseInt(aBuffer)).commit();
					myReader.close();
					Toast.makeText(getBaseContext(),
							"Done reading SD 'mysdfile.txt'",
							Toast.LENGTH_SHORT).show();
				} catch (Exception e) {
					Toast.makeText(getBaseContext(), e.getMessage()+" reading error",
							Toast.LENGTH_SHORT).show();
				}
				
				//Displaying a toast
				Toast.makeText(this,"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();

				//	new CheckMail().execute(getFileContent(root));

			}else{
				//Displaying another toast if permission is not granted
				Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
			}
		}



	}
*/

/*final AlertDialog alertDialog = new Builder(
MainActivity.this).create();

LayoutInflater inflater = getLayoutInflater();
View dialogView = inflater.inflate(R.layout.alertbox, null);
alertDialog.setView(dialogView);
TextView log = (TextView) dialogView.findViewById(R.id.textView1);
Button okay = (Button)  dialogView.findViewById(R.id.okay);
log.setText("Wabco updated Application available on Play Store \nDo you want to download?");
okay.setOnClickListener(new OnClickListener() {

@Override
public void onClick(View arg0) {
// TODO Auto-generated method stub

Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
intent.setData(Uri.parse("package:"+getPackageName()));
startActivity(intent);



if(isReadStorageAllowed())
{


}
else
{
requestStoragePermission();
}




final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
try {
	startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" +getPackageName())));
} catch (android.content.ActivityNotFoundException anfe) {
	startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
}


alertDialog.dismiss();
}
});
alertDialog.show();*/



/*  dbHelper = new DBHelper(MainActivity.this);
dbHelper.onOpen(db);

Cursor cursor = db.rawQuery("Select version from version", null);
if (cursor.moveToFirst()) {
do {
 version = cursor.getInt(cursor.getColumnIndex("version"));
 editor.putInt("VERSION", version).commit();
 
} while (cursor.moveToNext());
}*/