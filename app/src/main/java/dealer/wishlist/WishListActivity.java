package dealer.wishlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;

import adapter.View_wishlist_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import dealer.account.Dealer_Account_Activity;
import persistence.DBHelper;
import quickorder.Quick_Order_Preview_Activity;

public class WishListActivity extends Activity {

    private LinearLayout Default_wishlist_layout;
    private View heade_Layout;
    private ImageView Backbtn, Accountbtn,Cart_Icon,Menu;
    private TextView Tittle,Username;
    private Button Create_New,Remove;
    private SharedPreferences myshare;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private ListView listview;
    private ArrayList<String> Wishname,checkname;
    private ArrayList<Integer> Wishid;
    private ArrayList<Integer> checksid,checkspos;
    View_wishlist_Adapter Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        progressDialog = new ProgressDialog(WishListActivity.this);

        //Default_wishlist_layout = (LinearLayout) findViewById(R.id.default_wishlist_layout);
        listview = (ListView) findViewById(R.id.whishlist);

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        Menu = (ImageView) heade_Layout.findViewById(R.id.menu);


        Username =  (TextView) findViewById(R.id.username_wishlist);
        dbHelper = new DBHelper(WishListActivity.this);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        Wishname = new ArrayList<String>();
        Wishid = new ArrayList<Integer>();
        checksid=new ArrayList<Integer>();
        checkspos=new ArrayList<Integer>();
        checkname=new ArrayList<String>();
        Adapter  = new View_wishlist_Adapter(WishListActivity.this, Wishname,Wishid,checksid,checkspos,checkname);;

        Tittle.setText("Wish List");
        Username.setText(String.format("%s 's Wish List", myshare.getString("name", "Name")).toLowerCase());
        Create_New = (Button) findViewById(R.id.createnew);
        Remove = (Button) findViewById(R.id.remove);

        Accountbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(getApplicationContext(), Dealer_Account_Activity.class));
            }
        });

        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });


       /* listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent a = new Intent(WishListActivity.this, WishList_details_Activity.class);
                a.putExtra("ID", Wishid.get(position));
                a.putExtra("Listname", Wishname.get(position));
                startActivity(a);



            }
        });*/


        Create_New.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new MaterialDialog.Builder(WishListActivity.this)
                        .title("WABCO")
                        .content("Create new wish name")
                        .autoDismiss(false)
                        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
                        .input("Enter your wish name", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                            }
                        })
                        .positiveText("Create")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if(dialog.getInputEditText().length()!=0)
                                {
                                    CartDAO cartDAO = new CartDAO(WishListActivity.this);
                                    if(cartDAO.AddNewWishName(dialog.getInputEditText().getText().toString()))
                                    {
                                        dialog.dismiss();
                                        StyleableToast st = new StyleableToast(WishListActivity.this, "New wish name created successfully !  ", Toast.LENGTH_SHORT);
                                        st.setBackgroundColor(WishListActivity.this.getResources().getColor(R.color.green));
                                        st.setTextColor(Color.WHITE);
                                        st.setMaxAlpha();
                                        st.show();
                                        recreate();

                                    }
                                    else
                                    {
                                        StyleableToast st = new StyleableToast(WishListActivity.this, "Another wishlist already exists in the same name !  ", Toast.LENGTH_SHORT);
                                        st.setBackgroundColor(WishListActivity.this.getResources().getColor(R.color.red));
                                        st.setTextColor(Color.WHITE);
                                        st.setMaxAlpha();
                                        st.show();
                                    }
                                }
                                else
                                {
                                    StyleableToast st =
                                            new StyleableToast(WishListActivity.this, "Enter wish name !", Toast.LENGTH_SHORT);
                                    st.setBackgroundColor(getResources().getColor(R.color.red));
                                    st.setTextColor(Color.WHITE);
                                    st.setMaxAlpha();
                                    st.show();
                                }


                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checksid.size()==0)
                {
                    StyleableToast st = new StyleableToast(WishListActivity.this, "Select list to delete", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    delete(checksid);
                    for (int i = 0; i < checksid.size(); i++) {
                        Wishname.remove(checkname.get(i));
                        Wishid.remove(checksid.get(i));

                    }
                    Adapter.notifyDataSetChanged();
                    checksid.clear();
                    checkspos.clear();
                    checkname.clear();
                }


            }
        });


        new GetWishlist().execute();

    }
    private class GetWishlist extends AsyncTask<String, Void, String> {

        Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbHelper.readDataBase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = "select * from WishLists";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        Wishid.add(cursor.getInt(cursor.getColumnIndex("WishId")));
                        Wishname.add(cursor.getString(cursor.getColumnIndex("wishname")));


                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":


                    listview.setAdapter(Adapter);
                    break;
                case "nodata":

                    final AlertDialog.Builder alertDialog =
                            new AlertDialog.Builder(WishListActivity.this);
                    alertDialog.setTitle("WABCO");
                    alertDialog.setMessage("WishList is Empty");
                    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

                    alertDialog.show();

                    break;
                default:

                    break;
            }

        }

    }
    private void delete(ArrayList<Integer> IDlist)
    {

        SQLiteDatabase db = null;
        DBHelper dbhelper = new DBHelper(WishListActivity.this);
        db = dbhelper.readDataBase();
        for (int i = 0; i < IDlist.size(); i++)
        {
            db.execSQL("delete from WishLists where WishId='" + IDlist.get(i) + "'");
            db.execSQL("delete from WishItems where WishId='" + IDlist.get(i) + "'");
        }
        db.close();
    }
}
