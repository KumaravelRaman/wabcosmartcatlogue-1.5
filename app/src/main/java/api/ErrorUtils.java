package api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import models.errormodel.APIError;
import okhttp3.ResponseBody;
import registration.RegisterActivity;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by system01 on 6/8/2017.
 */

public class ErrorUtils {

    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                RegisterActivity.retrofit
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }
}
