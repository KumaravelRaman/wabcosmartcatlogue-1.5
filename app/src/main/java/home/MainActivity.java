package home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.viewpagerindicator.CirclePageIndicator;
import com.wabco.brainmagic.wabco.catalogue.EquivalentActivity;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import distributor.account.Distributor_Account_Activity;
import geniunecheck.activities.GenuineCheck;
import geniunecheck.activities.OriginalParts;
import login.UserLoginActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Activity;
import search.SearchActivity;
import serviceengineer.account.FieldEngineer_Account;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


@SuppressLint({"SdCardPath"})
public class MainActivity extends Activity {
  private RelativeLayout PE_kit_RelaytiveLayout, Update_RelaytiveLayout, Network_RelaytiveLayout,
      PriceList_RelaytiveLayout;
  private RelativeLayout Scan_layout,equvalent_relativelayout;
  private View contact_RelaytiveLayout;
  private SharedPreferences.Editor editor;
  private RelativeLayout notification_RelaytiveLayout;
  private SharedPreferences preferences;
  private RelativeLayout productFamilyRelativeLayout;
  private RelativeLayout search_RelaytiveLayout;
  private ProgressDialog updateCheckDialog;
  private RelativeLayout vechilemakeRelativeLayout,getEquvalent_Relativelayout;
  private SQLiteDatabase db;
  private DBHelper dbHelper;
  private int localvertion = 1;
  protected HomePage atualizaApp;
  private TableRow t1;

  public Connection connection;
  public Statement stmt;
  public ResultSet rset;
  public int version = 0;
  private int STORAGE_PERMISSION_CODE = 23;
  private static final int REQUEST_WRITE_STORAGE = 112;
  String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
      .getAbsolutePath().toString() + "/mysdfile.txt";
  public String apkurl = "http://brainmagic.info/MobileApps/wabco/db/wabco.apk";

  int noofsize = 5;
  ViewPager myPager = null;
  int count = 0;
  Timer timer;
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private static int currentPage = 0;
  private static int NUM_PAGES = 3;
  private ImageView loginIcon,equvalentimg;
  private TextView loginText,equvalenttxt;
  private String UserType;

  @RequiresApi(api = Build.VERSION_CODES.M)
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_main);
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());

    // isReadStorageAllowed();


    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();
    UserType = myshare.getString("usertype", "").toString();
    // checkVersion();

    t1 =(TableRow)findViewById(R.id.asc1);
    t1.setVisibility(View.GONE);
    // Login for dealer'
    loginIcon = (ImageView) findViewById(R.id.update_imageView);
    loginText = (TextView) findViewById(R.id.update_text);
  //  equvalentimg = (ImageView)findViewById(R.id.equvalent_imageView);
   // equvalenttxt = (TextView)findViewById(R.id.equvalent_text);

    if (UserType.equals("Dealer") || UserType.equals("OEM Dealer") ||UserType.equals("ASC")) {
      /*
       * if(!myshare.getBoolean("islogin",false)) { loginIcon.setImageResource(R.drawable.login);
       * loginText.setText("ORDER"); } else { loginIcon.setImageResource(R.drawable.account);
       * loginText.setText("YOUR ACCOUNT"); }
       */
      loginIcon.setImageResource(R.drawable.addtocart);
      loginText.setText("ORDER");
    } /*else if(UserType.equals("Wabco employee")){
      loginIcon.setImageResource(R.drawable.account);
      loginText.setText("YOUR ACCOUNT");
    }*/
    else if(UserType.equals("ASC")){
        t1.setVisibility(View.VISIBLE);
    }
      else {
      loginIcon.setImageResource(R.drawable.update);
      loginText.setText("CHECK \nUPDATE");
    }
    vechilemakeRelativeLayout = ((RelativeLayout) findViewById(R.id.vechile_relativelayout));
    vechilemakeRelativeLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, VehicleMakeActivity.class);
        startActivity(localIntent);
      }
    });
    productFamilyRelativeLayout = ((RelativeLayout) findViewById(R.id.product_relativelayout));
    productFamilyRelativeLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, ProductFamilyActivity.class);
        startActivity(localIntent);
      }
    });
    search_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.search_relativelayout));
    search_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, SearchActivity.class);
        startActivity(localIntent);
      }
    });
    contact_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.contact_relativelayout));
    contact_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, AskWabcoActivity.class);
        startActivity(localIntent);
      }
    });
    notification_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.notification));
    notification_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, NotificationActivity.class);
        startActivity(localIntent);
      }
    });
    PE_kit_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.performance_relativelayout));
    PE_kit_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, PE_Kit_Activity.class);
        startActivity(localIntent);
      }
    });


    PriceList_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.priceList));
    PriceList_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, PriceListActivity.class);
        startActivity(localIntent);
      }
    });

    Network_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.network));
    Network_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        Intent localIntent = new Intent(MainActivity.this, Network_Activity.class);
        startActivity(localIntent);
      }
    });
      equvalent_relativelayout = ((RelativeLayout) findViewById(R.id.equvalent_relativelayout));

      equvalent_relativelayout.setOnClickListener(new OnClickListener() {
          public void onClick(View view) {
              startActivity(new Intent(MainActivity.this, EquivalentActivity.class));
          }
      });

    Update_RelaytiveLayout = ((RelativeLayout) findViewById(R.id.update_relativelayout));
    Update_RelaytiveLayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        if(UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
          ShowSelection();
            else
                ShowSelection();
        } else if (UserType.equals("Wabco Employee")) {

          if (!myshare.getBoolean("islogin", false)) {
            startActivity(
                    new Intent(MainActivity.this, UserLoginActivity.class).putExtra("from", "home"));
          } else {
            startActivity(new Intent(MainActivity.this, FieldEngineer_Account.class));
          }
        }  else {
          WabcoUpdate update = new WabcoUpdate(MainActivity.this);
          update.checkVersion();
        }
      }
    });
    Scan_layout = ((RelativeLayout) findViewById(R.id.scan_layout));
    Scan_layout.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(!myshare.getBoolean("isScanned",false)) {
          Intent mainIntent = new Intent(MainActivity.this, GenuineCheck.class);
          startActivity(mainIntent);
        }
        else
        {
          Intent mainIntent = new Intent(MainActivity.this, OriginalParts.class);
          startActivity(mainIntent);
        }
      }
    });

    if (UserType.equals("ASC") ||
            UserType.equals("Others")) {
          Scan_layout.setVisibility(View.GONE);
    }
    else
    {
      Scan_layout.setVisibility(View.VISIBLE);
    }


   /* Equvalent_Relativelayout = ((RelativeLayout) findViewById(R.id.equvalent_relativelayout));
    Equvalent_Relativelayout.setVisibility(View.GONE);
    Equvalent_Relativelayout.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {

        startActivity(new Intent(MainActivity.this, EquivalentActivity.class));

      }
    });*/



    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(MainActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnDismissListener(new OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });
        pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.search:
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(MainActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(MainActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(MainActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(MainActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(MainActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(MainActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(MainActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.inflate(R.menu.main);
        pop.show();
      }
    });


    MyCustomPagerAdapter adapter = new MyCustomPagerAdapter(MainActivity.this);
    myPager = (ViewPager) findViewById(R.id.viewpager);

    CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

    myPager.setAdapter(adapter);
    myPager.setCurrentItem(0);

    final float density = getResources().getDisplayMetrics().density;
    indicator.setViewPager(myPager);
    indicator.setRadius(5 * density);


    // Auto start of viewpager
    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
      public void run() {
        if (currentPage == NUM_PAGES) {
          currentPage = 0;
        }
        myPager.setCurrentItem(currentPage++, true);
      }
    };
    Timer swipeTimer = new Timer();
    swipeTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(Update);
      }
    }, 3000, 3000);


    // Pager listener over indicator
    indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

      @Override
      public void onPageSelected(int position) {
        currentPage = position;

      }

      @Override
      public void onPageScrolled(int pos, float arg1, int arg2) {

      }

      @Override
      public void onPageScrollStateChanged(int pos) {

      }
    });
      DBHelper dbhelper = new DBHelper(MainActivity.this);
      db = dbhelper.readDataBase();
  }
  // Requesting permission
  public boolean isReadStorageAllowed() {
    // Getting the permission status
    int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

    // If permission is granted returning true
    if (result == PackageManager.PERMISSION_GRANTED) {

      return true;

    } else {
      ActivityCompat.requestPermissions(this,
          new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }
    return false;


  }


  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (requestCode == REQUEST_WRITE_STORAGE) {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


      } else {
        Toast.makeText(MainActivity.this,
            "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission",
            Toast.LENGTH_LONG).show();
        isReadStorageAllowed();
      }
    } else if (requestCode == STORAGE_PERMISSION_CODE) {
      // If permission is granted
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        // Displaying a toast
        Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG)
            .show();

        // new CheckMail().execute(getFileContent(root));

      } else {
        // Displaying another toast if permission is not granted
        Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
      }
    }


  }

  private void ShowSelection() {

   new BottomSheet.Builder(MainActivity.this).setSheet(R.menu.custom_menu).grid().setColumnCount(2)
        .setTitle("Select options").setListener(new BottomSheetListener() {
          @Override
          public void onSheetShown(@NonNull BottomSheet bottomSheet) {
        }
          @Override
          public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem) {

            if (menuItem.getTitle().equals("Quick Order")) {
              startActivity(new Intent(MainActivity.this, Quick_Order_Activity.class));
            } else {
              if (!myshare.getBoolean("islogin", false)) {
                startActivity(
                        new Intent(MainActivity.this, UserLoginActivity.class).putExtra("from", "home"));
              } else {
                if (UserType.equals("Whole Sale Distributor")) {
                  startActivity(new Intent(MainActivity.this, Distributor_Account_Activity.class));
                } /*else if (UserType.equals("Wabco employee")) {
                  startActivity(new Intent(MainActivity.this, FieldEngineer_Account.class));
                } */else {
                  startActivity(new Intent(MainActivity.this, Dealer_Account_Activity.class));
                }
              }
            }
          }

          @Override
          public void onSheetDismissed(@NonNull BottomSheet bottomSheet,
              @BottomSheetListener.DismissEvent int i) {

        }
        }).show();


/*
    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

    LayoutInflater inflater = getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.custom_dialog, null);
    alertDialog.setView(dialogView);
    alertDialog.setTitle("Wabco");
    LinearLayout quick = (LinearLayout) dialogView.findViewById(R.id.quick_order);
    LinearLayout catalogue = (LinearLayout) dialogView.findViewById(R.id.catalogue_order);
    LinearLayout account = (LinearLayout) dialogView.findViewById(R.id.account_layout);

    quick.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View arg0) {
        // TODO Auto-generated method stub
        alertDialog.dismiss();
        startActivity(new Intent(MainActivity.this, Quick_Order_Activity.class));
      }
    });
    alertDialog.show();
    catalogue.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View arg0) {
        // TODO Auto-generated method stub
        alertDialog.dismiss();
        startActivity(new Intent(MainActivity.this, MainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | (Intent.FLAG_ACTIVITY_NEW_TASK)));

      }
    });

    account.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        alertDialog.dismiss();

        if (!myshare.getBoolean("islogin", false)) {
          startActivity(
              new Intent(MainActivity.this, UserLoginActivity.class).putExtra("from", "home"));
        } else {
          if (UserType.equals("Whole Sale Distributor")) {
            startActivity(new Intent(MainActivity.this, Distributor_Account_Activity.class));
          } else if (UserType.equals("Wabco employee")) {
            startActivity(new Intent(MainActivity.this, FieldEngineer_Account.class));
          } else {
            startActivity(new Intent(MainActivity.this, Dealer_Account_Activity.class));
          }
        }


      }
    });

    alertDialog.show();*/

  }

  private void showAlertBox(String msg) {
    // TODO Auto-generated method stub
    Alertbox box = new Alertbox(MainActivity.this);
    box.showAlertbox(msg);
  }


}

