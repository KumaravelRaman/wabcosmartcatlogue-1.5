package search;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import addcart.CartDTO;
import persistence.DBHelper;


public class SearchDAO {
  private SQLiteDatabase db;
  private DBHelper dbHelper;


  public SearchDAO(Context context)
  {
    this.dbHelper = new DBHelper(context);
  }

  public void openDatabase()
  {
    this.db = this.dbHelper.readDataBase();
  }

  public void closeDatabase() {
    if (this.db != null) {
      this.db.close();
    }
  }

  public SearchDTO retrieveALLPartNo() {
    List<String> partIDList = new ArrayList<String>();
    SearchDTO vehicleDAO = new SearchDTO();
    openDatabase();
    Cursor cursor = this.db.rawQuery("select distinct Part_No from products_assemply", null);
    if (cursor.moveToFirst()) {
      do {
        partIDList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    vehicleDAO.setPartno_List(partIDList);
    return vehicleDAO;
  }

  public SearchDTO retriveForVehicleForshowAll(String item) {
    String productNameList = null;
    List<String> vehicleNameList = new ArrayList<String>();
    List<String> vehicleIDList = new ArrayList<String>();
    List<String> productPartno_List = new ArrayList<String>();
    List<String> Description_List = new ArrayList<String>();
    List<String> image_List = new ArrayList<String>();
    List<String> flag_List = new ArrayList<String>();
    List<String> productIDList = new ArrayList<String>();
    SearchDTO searchDTO = new SearchDTO();
    Log.v(" Changed", item);
    openDatabase();
    Cursor cursor = this.db.rawQuery(
        "select distinct v.Vehicle_id,v.Vehicle_name,p.Part_No,p.Description,p.ProductImage,p.Vehicle_Product_id,p.flag,u.ProducutName from products_assemply p,vehicle_make v,product u where v.Vehicle_id = p.Vehicle_id  and p.Vehicle_Product_id = u.Vehicle_Product_id and p.Part_No like'%"
            + item + "%'",
        null);
    if (cursor.moveToFirst()) {
      do {
        vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
        productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
        Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
        image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
        productNameList = cursor.getString(cursor.getColumnIndex("ProducutName"));
        flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
        vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
        productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setVehicleNameList(vehicleNameList);
    searchDTO.setPart_No_List(productPartno_List);
    searchDTO.setDescription(Description_List);
    searchDTO.setService_Part(image_List);
    searchDTO.setProductNameList(productNameList);
    searchDTO.setFlagList(flag_List);
    searchDTO.setVehicleIDList(vehicleIDList);
    searchDTO.setProductIdList(productIDList);
    return searchDTO;
  }


  private String SetFlag(String partno) {
    // TODO Auto-generated method stub
    openDatabase();

    String query = "select Part_No from pricelist where  Part_No ='" + partno + "' or Partcode  ='"
        + partno + "'";

    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return "0";
    } else {
      return "1";
    }

  }
  private String SetFlagSearch(String partno) {
    // TODO Auto-generated method stub
    //openDatabase();

    String query = "select Part_No from pricelist where  Part_No ='" + partno + "' or Partcode  ='"
            + partno + "'";

    Cursor cur = this.db.rawQuery(query, null);
    if (cur.moveToFirst()) {
      return "0";
    } else {
      return "1";
    }

  }
  public SearchDTO retriveForDescription(String item) {
    String productNameList = null;
    List<String> vehicleNameList = new ArrayList<String>();
    List<String> vehicleIDList = new ArrayList<String>();
    List<String> productPartno_List = new ArrayList<String>();
    List<String> Description_List = new ArrayList<String>();
    List<String> image_List = new ArrayList<String>();
    List<String> flag_List = new ArrayList<String>();
    List<String> productIDList = new ArrayList<String>();
    SearchDTO searchDTO = new SearchDTO();
    Log.v(" Changed", item);
    openDatabase();
    Cursor cursor = this.db.rawQuery(
        "select distinct v.Vehicle_id,v.Vehicle_name,p.Part_No,p.Description,p.ProductImage,p.Vehicle_Product_id,u.ProducutName,p.flag from products_assemply p,vehicle_make v,product u where v.Vehicle_id = p.Vehicle_id  and p.Vehicle_Product_id = u.Vehicle_Product_id and p.Description like'%"
            + item + "%'",
        null);

    Log.v("Description ", "");
    if (cursor.moveToFirst()) {
      do {
        vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
        productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
        Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
        image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
        productNameList = cursor.getString(cursor.getColumnIndex("ProducutName"));
        flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
        vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
        productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setVehicleNameList(vehicleNameList);
    searchDTO.setPart_No_List(productPartno_List);
    searchDTO.setDescription(Description_List);
    searchDTO.setService_Part(image_List);
    searchDTO.setProductNameList(productNameList);
    searchDTO.setFlagList(flag_List);
    searchDTO.setVehicleIDList(vehicleIDList);
    searchDTO.setProductIdList(productIDList);
    return searchDTO;
  }

  public SearchDTO retrieveALLServicePartNo(String type) {
    List<String> partIDList = new ArrayList<String>();
    SearchDTO vehicleDAO = new SearchDTO();
    openDatabase();
    Cursor cursor;
    if (type.equals("Service Part-No")) {
      cursor = this.db.rawQuery("select distinct Service_part from products_servicepart", null);
      if (cursor.moveToFirst()) {
        do {
          partIDList.add(cursor.getString(cursor.getColumnIndex("Service_part")));
        } while (cursor.moveToNext());
      }
    } else {
      cursor = this.db.rawQuery("select distinct Repair_Part_Number from products_repairkit", null);
      if (cursor.moveToFirst()) {
        do {
          partIDList.add(cursor.getString(cursor.getColumnIndex("Repair_Part_Number")));
        } while (cursor.moveToNext());
      }
    }
    closeDatabase();
    vehicleDAO.setPartno_List(partIDList);
    return vehicleDAO;
  }

  public SearchDTO retriveForServiceAll(String item, String type) {
    List<String> vehiclePartNoList = new ArrayList<String>();
    List<String> vehicleDescriptionList = new ArrayList<String>();
    List<String> vehicleReparKitNo = new ArrayList<String>();
    List<String> vehicleNameList = new ArrayList<String>();
    List<String> productNameList = new ArrayList<String>();
    List<String> vehicleIDList = new ArrayList<String>();
    List<String> productIDList = new ArrayList<String>();
    List<String> flagList = new ArrayList<String>();
    openDatabase();
    SearchDTO vehicleDTO = new SearchDTO();
    Cursor cursor;
    if (type.equals("Service Part No")) {
      cursor = this.db.rawQuery(
          "select distinct  ps.Part_No,ps.Service_part,ps.Description,n.ProducutName ,m.Vehicle_id,m.Vehicle_name,n.Vehicle_Product_id from products_servicepart ps,product n,vehicle_make m where n.Vehicle_Product_id = ps.Vehicle_Product_id and m.Vehicle_id= ps.Vehicle_id and  Service_part like'%"
              + item + "%'",
          null);
      if (cursor.moveToFirst()) {
        do {
          Log.v("Search query",
              "select distinct  ps.Part_No,ps.Service_part,ps.Description,n.ProducutName ,m.Vehicle_id,m.Vehicle_name,n.Vehicle_Product_id from products_servicepart ps,product n,vehicle_make m where n.Vehicle_Product_id = ps.Vehicle_Product_id and m.Vehicle_id= ps.Vehicle_id and  Service_part like'%"
                  + item + "%'");
          vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
          vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Service_part")));
          vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
          vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
          vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
          productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
          productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
          flagList.add(SetFlagSearch(cursor.getString(cursor.getColumnIndex("Service_part"))));

        } while (cursor.moveToNext());
      }
    } else {
      cursor = this.db.rawQuery(
          "select distinct  ps.Part_No,ps.Repair_Part_Number,ps.Description,n.ProducutName ,m.Vehicle_id,m.Vehicle_name,n.Vehicle_Product_id from products_repairkit ps,product n,vehicle_make m where n.Vehicle_Product_id = ps.Vehicle_Product_id and m.Vehicle_id= ps.Vehicle_id and Repair_Part_Number like'%"
              + item + "%'",
          null);
      if (cursor.moveToFirst()) {
        do {
          vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
          vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Repair_Part_Number")));
          vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
          vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
          vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
          productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
          productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
          flagList.add(SetFlagSearch(cursor.getString(cursor.getColumnIndex("Repair_Part_Number"))));
        } while (cursor.moveToNext());
      }
}
    closeDatabase();
    vehicleDTO.setPart_No_List(vehicleReparKitNo);
    vehicleDTO.setDescription(vehicleDescriptionList);
    vehicleDTO.setPart_A(vehiclePartNoList);
    vehicleDTO.setProductList(productNameList);
    vehicleDTO.setProductIdList(productIDList);
    vehicleDTO.setVehicleNameList(vehicleNameList);
    vehicleDTO.setVehicleIDList(vehicleIDList);
    vehicleDTO.setPriceflaglist(flagList);
    return vehicleDTO;
  }

  public SearchDTO retriveForServiceDescription(String item, String type) {
    List<String> vehiclePartNoList = new ArrayList<String>();
    List<String> vehicleDescriptionList = new ArrayList<String>();
    List<String> vehicleReparKitNo = new ArrayList<String>();
    List<String> vehicleNameList = new ArrayList<String>();
    List<String> productNameList = new ArrayList<String>();
    List<String> vehicleIDList = new ArrayList<String>();
    List<String> productIDList = new ArrayList<String>();
    List<String> priceflagList = new ArrayList<String>();

    openDatabase();
    SearchDTO vehicleDTO = new SearchDTO();
    Cursor cursor;
    if (type.equals("Service Part-No")) {
      cursor = this.db.rawQuery(
          "select distinct  ps.Part_No,ps.Service_part,ps.Description,n.ProducutName ,m.Vehicle_id,m.Vehicle_name,n.Vehicle_Product_id from products_servicepart ps,product n,vehicle_make m where n.Vehicle_Product_id = ps.Vehicle_Product_id and m.Vehicle_id= ps.Vehicle_id and  description like'%"
              + item + "%'",
          null);
      if (cursor.moveToFirst()) {
        do {
          vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
          vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Service_part")));
          vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
          vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
          vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
          productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
          productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
          priceflagList.add(SetFlagSearch(cursor.getString(cursor.getColumnIndex("Service_part"))));
        } while (cursor.moveToNext());
      }
    } else {
      cursor = this.db.rawQuery(
          "select distinct  ps.Part_No,ps.Repair_Part_Number,ps.Description,n.ProducutName ,m.Vehicle_id,m.Vehicle_name,n.Vehicle_Product_id from products_repairkit ps,product n,vehicle_make m where n.Vehicle_Product_id = ps.Vehicle_Product_id and m.Vehicle_id= ps.Vehicle_id and Description like'%"
              + item + "%'",
          null);
      if (cursor.moveToFirst()) {
        do {
          vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
          vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Repair_Part_Number")));
          vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
          vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
          vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
          productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
          productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
          priceflagList.add(SetFlagSearch(cursor.getString(cursor.getColumnIndex("Repair_Part_Number"))));

        } while (cursor.moveToNext());
      }
    }
    closeDatabase();
    vehicleDTO.setPart_No_List(vehicleReparKitNo);
    vehicleDTO.setDescription(vehicleDescriptionList);
    vehicleDTO.setPart_A(vehiclePartNoList);
    vehicleDTO.setProductList(productNameList);
    vehicleDTO.setProductIdList(productIDList);
    vehicleDTO.setVehicleNameList(vehicleNameList);
    vehicleDTO.setVehicleIDList(vehicleIDList);
    vehicleDTO.setPriceflaglist(priceflagList);
    return vehicleDTO;
  }

  public SearchDTO retriveForPEkit(String item) {
    List<String> PartnoList = new ArrayList<String>();
    List<String> Partcodeno = new ArrayList<String>();
    List<String> Descrip = new ArrayList<String>();
    List<String> flaglist = new ArrayList<String>();
    List<String> priceflaglist = new ArrayList<String>();
    SearchDTO searchDTO = new SearchDTO();
    Log.v(" Changed", item);
    openDatabase();
    Cursor cursor = this.db.rawQuery(
        "select distinct  part_no,part_code_no,Description,used from pekit where part_no like'%"
            + item + "%'",
        null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("part_no")));
        Partcodeno.add(cursor.getString(cursor.getColumnIndex("part_code_no")));
        Descrip.add(cursor.getString(cursor.getColumnIndex("Description")));
        flaglist.add(cursor.getString(cursor.getColumnIndex("used")));
        priceflaglist.add(SetFlag(cursor.getString(cursor.getColumnIndex("part_no"))));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setPartNoList(PartnoList);
    searchDTO.setPartDespcriptionList(Partcodeno);
    searchDTO.setProductservicepartNOList(Descrip);
    searchDTO.setFlagList(flaglist);
    searchDTO.setPriceflaglist(priceflaglist);
    return searchDTO;
  }


  public SearchDTO retriveForPriceList(String word, String from, String partordesc) {
    String query;
    List<String> PartnoList = new ArrayList<String>();
    List<String> Partcodeno = new ArrayList<String>();
    List<String> Descrip = new ArrayList<String>();
    List<String> Price_List = new ArrayList<String>();
    List<String> MrpList = new ArrayList<String>();
    List<String> CatagoryList = new ArrayList<String>();
    List<String> ImageList = new ArrayList<String>();

    SearchDTO searchDTO = new SearchDTO();
    openDatabase();
    if(from.equals("All"))
    {
      if (word.length() == 0) {
        query = "select distinct  *  from pricelist ";

      }
      else if (partordesc.equals("part")) {
//        query = "select distinct  *  from pricelist where Part_No like'%" + word + "%'";
        query = "select distinct  *  from pricelist where Partcode like'%" + word + "%'";
      }
      else {
        query = "select distinct  *  from pricelist where  description  like '%" + word + "%'";

      }
    }
    else
      {
      if (word.length() == 0) {
//        query = "select distinct  *  from pricelist where  category = '" + from + "'";
        query = "select distinct  *  from pricelist ";
      }
      else if(partordesc.equals("part") &&partordesc.equals("desc") ){
        query = "select distinct  *  from pricelist where  Partcode = '" + from + "' or description like'%" + word + "%'";
      }
      else if (partordesc.equals("part")) {
//        query = "select distinct  *  from pricelist where  category = '" + from
//                + "' and Part_No like'%" + word + "%'";

        query = "select distinct  *  from pricelist where  Partcode = '" + word + "' ";

//        query = "select distinct  *  from pricelist where  Partcode = '" + from + "' or description like'%" + word + "%'";

      }
      else if (partordesc.equals("desc")) {
//        query = "select distinct  *  from pricelist where  category = '" + from
//                + "' and Part_No like'%" + word + "%'";

//        query = "select distinct  *  from pricelist where  description like'%" + word + "%'";
        query = "select distinct  *  from pricelist where  Partcode = '" + from + "' or description like'%" + word + "%'";

      }

      else {
//        query = "select distinct  *  from pricelist where  category = '" + from
//                + "' and  description  like '%" + word + "%'";
        query = "select distinct  *  from pricelist where  Partcode = '" + from
                + "' and  description  like '%" + word + "%'";

      }
    }
    Log.v("query", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
        Partcodeno.add(cursor.getString(cursor.getColumnIndex("Partcode")));
        Descrip.add(cursor.getString(cursor.getColumnIndex("description")));
        Price_List.add(cursor.getString(cursor.getColumnIndex("pricelist")));
        MrpList.add(cursor.getString(cursor.getColumnIndex("mrplist")));
        CatagoryList.add(cursor.getString(cursor.getColumnIndex("category")));
        ImageList.add(cursor.getString(cursor.getColumnIndex("image")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setPartNoList(PartnoList);
    searchDTO.setDespcriptionList(Descrip);
    searchDTO.setpartCodeList(Partcodeno);
    searchDTO.setPrice_List(Price_List);
    searchDTO.setMrpList(MrpList);
    searchDTO.setCatagoryList(CatagoryList);
    searchDTO.setImageList(ImageList);

    return searchDTO;
  }



  public SearchDTO PricePartNoList(String from) {
    String query;
    ArrayList<String> PartnoList = new ArrayList<String>();


    SearchDTO searchDTO = new SearchDTO();
    openDatabase();
if (from.equals("All"))
{
  query = "select distinct  *  from pricelist";
}
else
{
//  query = "select distinct  *  from pricelist where  category = '" + from + "'";
//  query = "select distinct  *  from pricelist where  Partcode = '" + from + "'";
  query = "select distinct  *  from pricelist";

}


    Log.v("query", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("Partcode")));

      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setAutocompletepartcode(PartnoList);

    return searchDTO;
  }


  public SearchDTO PriceDescList(String from) {
    String query;

    ArrayList<String> Descrip = new ArrayList<String>();

    SearchDTO searchDTO = new SearchDTO();
    openDatabase();

    if (from.equals("All"))
    {
      query = "select distinct  *  from pricelist";
    }
    else
    {
//      query = "select distinct  *  from pricelist where  category = '" + from + "'";
      query = "select distinct  *  from pricelist";
    }

    Log.v("query", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        Descrip.add(cursor.getString(cursor.getColumnIndex("description")));

      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setAutocompleteDespcriptionList(Descrip);

    return searchDTO;
  }


  public SearchDTO PricePartNoListForQuickOrder() {
    String query;
    ArrayList<String> PartnoList = new ArrayList<String>();


    SearchDTO searchDTO = new SearchDTO();
    openDatabase();

    query = "select distinct  *  from pricelist";

    Log.v("query", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("Partcode")));

      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setAutocompletePartNoList(PartnoList);

    return searchDTO;
  }

  public String PriceDescriptionForQuickOrder(String part) {
    String query;
    String Partno = "";

    openDatabase();
    query = "select distinct  *  from pricelist where Part_No = '" + part + "' or Partcode = '" + part + "' ";
    Log.v("query", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        Partno = cursor.getString(cursor.getColumnIndex("description"));

      } while (cursor.moveToNext());
    }
    closeDatabase();


    return Partno;
  }


  public CartDTO retriveForPriceQuickOrder(String partno) {
    String query = "";
    List<String> PartnoList = new ArrayList<String>();
    List<String> Partcodeno = new ArrayList<String>();
    List<String> Descrip = new ArrayList<String>();
    List<String> Price_List = new ArrayList<String>();
    List<String> MrpList = new ArrayList<String>();
    List<String> CatagoryList = new ArrayList<String>();
    List<String> ImageList = new ArrayList<String>();
    List<String> PartnameList = new ArrayList<String>();

    CartDTO cartDTO = new CartDTO();
    openDatabase();
    if (partno.length() != 0) {
      query = "select distinct  *  from pricelist where  Part_No = '" + partno + "' or Partcode = '" + partno + "'";
      Log.v("query", query);
    }

    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
        Partcodeno.add(cursor.getString(cursor.getColumnIndex("Partcode")));
        PartnameList.add(cursor.getString(cursor.getColumnIndex("description")));
        Descrip.add(cursor.getString(cursor.getColumnIndex("description")));
        Price_List.add(cursor.getString(cursor.getColumnIndex("pricelist")));
        MrpList.add(cursor.getString(cursor.getColumnIndex("mrplist")));
        CatagoryList.add(cursor.getString(cursor.getColumnIndex("category")));
        ImageList.add(cursor.getString(cursor.getColumnIndex("image")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    cartDTO.setPartNoList(PartnoList);
    cartDTO.setPartNameList(PartnameList);
    cartDTO.setPartdescriptionList(Descrip);
    cartDTO.setPartCodeList(Partcodeno);
    cartDTO.setPartPriceList(MrpList);
    cartDTO.setPartTypeList(CatagoryList);
    cartDTO.setPartImageList(ImageList);

    return cartDTO;
  }

  public CartDTO retriveForCartPriceList() {
    String query = "";
    List<String> PartnoList = new ArrayList<String>();
    List<String> Partcodeno = new ArrayList<String>();
    List<String> Descrip = new ArrayList<String>();
    List<String> Price_List = new ArrayList<String>();
    List<String> MrpList = new ArrayList<String>();
    List<String> CatagoryList = new ArrayList<String>();
    List<String> ImageList = new ArrayList<String>();

    CartDTO cartDTO = new CartDTO();
    openDatabase();

    query = "select distinct  *  from pricelist";
    Log.v("query", query);


    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        PartnoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
        Partcodeno.add(cursor.getString(cursor.getColumnIndex("Partcode")));
        Descrip.add(cursor.getString(cursor.getColumnIndex("description")));
        Price_List.add(cursor.getString(cursor.getColumnIndex("pricelist")));
        MrpList.add(cursor.getString(cursor.getColumnIndex("mrplist")));
        CatagoryList.add(cursor.getString(cursor.getColumnIndex("category")));
        ImageList.add(cursor.getString(cursor.getColumnIndex("image")));
      } while (cursor.moveToNext());
    }
    closeDatabase();
    cartDTO.setPartNoList(PartnoList);
    cartDTO.setPartdescriptionList(Descrip);
    cartDTO.setPartCodeList(Partcodeno);
    cartDTO.setPartPriceList(Price_List);
    cartDTO.setPartTypeList(CatagoryList);
    cartDTO.setPartImageList(ImageList);

    return cartDTO;
  }


  public SearchDTO retriveForEquivalentPart(String item) {
    String productNameList = null;
    List<String> originalList = new ArrayList<String>();
    List<String> equivalentList = new ArrayList<String>();

    SearchDTO searchDTO = new SearchDTO();
    Log.v(" EquivalentPart", item);
    openDatabase();
    Cursor cursor = this.db.rawQuery("select distinct  partno,equivalent from  equivalent where partno like '%" + item + "%' or equivalent like '%" + item + "%'  ", null);
    if (cursor.moveToFirst()) {
      do {
        originalList.add(cursor.getString(cursor.getColumnIndex("partno")));
        equivalentList.add(cursor.getString(cursor.getColumnIndex("equivalent")));

      } while (cursor.moveToNext());
    }
    closeDatabase();
    searchDTO.setoriginalList(originalList);
    searchDTO.setequivalentList(equivalentList);

    return searchDTO;
  }




}
