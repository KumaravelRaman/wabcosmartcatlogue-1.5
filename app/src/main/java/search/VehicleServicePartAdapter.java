package search;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import addcart.Add_TocartActivity;
import vehiclemake.VehiclePartItemActivity;

import static android.content.Context.MODE_PRIVATE;

public class VehicleServicePartAdapter extends ArrayAdapter<String> {
    private List<String> Partno;
    Context context;
    private List<String> description;
    private List<String> service_partno;
	private List<String> VehicleID;
	private List<String> VehicleName;
	private List<String> ProductID;
	private List<String> ProductName;
    private List<String> priceflagList;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType,SearchFrom;
    //if (SearchFrom.equals("service")) {


    class ViewHolderservice {
        TextView assem;
        TextView desc;
        TextView servicepartno;
        TextView sno;
        Button cart;
        ViewHolderservice() {
        }
    }

    public VehicleServicePartAdapter(Context context, List<String> service_partno, List<String> description, List<String> Partno
    		,List<String> VehicleID,List<String> VehicleName,List<String> ProductID,List<String> ProductName,List<String>  priceflagList, String SearchFrom) {
        super(context, R.layout.activity_productfamily_servicepart_text_item, service_partno);
        this.VehicleID=VehicleID;
        this.VehicleName=VehicleName;
        this.ProductID=ProductID;
        this.ProductName=ProductName;
        this.service_partno = service_partno;
        this.description = description;
        this.context = context;
        this.Partno = Partno;
        this.priceflagList = priceflagList;
        this.SearchFrom = SearchFrom;
        myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        UserType = myshare.getString("usertype","").toString();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderservice viewHolderservice;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
            		.inflate(R.layout.activity_productfamily_servicepart_text_item, null);
            viewHolderservice = new ViewHolderservice();
            viewHolderservice.sno = (TextView) convertView.findViewById(R.id.sno);
            viewHolderservice.servicepartno = (TextView) convertView.findViewById(R.id.serno);
            viewHolderservice.desc = (TextView) convertView.findViewById(R.id.des);
            viewHolderservice.assem = (TextView) convertView.findViewById(R.id.Ass);
            viewHolderservice.cart = (Button) convertView.findViewById(R.id.addcart);

            convertView.setTag(viewHolderservice);
        } else {
            viewHolderservice = (ViewHolderservice) convertView.getTag();
        }
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")) && (priceflagList.get(position)).equals("0"))
        {
            viewHolderservice.cart.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolderservice.cart.setVisibility(View.GONE);
        }
        viewHolderservice.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        viewHolderservice.servicepartno.setText((CharSequence) this.service_partno.get(position));
        viewHolderservice.desc.setText((CharSequence) this.description.get(position));
        viewHolderservice.assem.setText((CharSequence) this.Partno.get(position));
        viewHolderservice.assem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent gotoassmply = new Intent(context, VehiclePartItemActivity.class);
				gotoassmply.putExtra("VEHICLE_ID", VehicleID.get(position));
				gotoassmply.putExtra("VEHICLE_NAME", VehicleName.get(position));
				gotoassmply.putExtra("PRODUCT_NAME", ProductName.get(position));
				gotoassmply.putExtra("VEHICLE_PRODUCTFAMILY_ID", ProductID.get(position));
				Log.v("PRODUCT_NAME", ProductName.get(position));
				Log.v("PRODUCT_ID", ProductID.get(position));
				Log.v("VEHICLE_NAME", VehicleName.get(position));
				Log.v("VEHICLE_ID", VehicleID.get(position));
				
		           context.startActivity(gotoassmply);
		        }
		});
        viewHolderservice.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent gotocart = new Intent(context, Add_TocartActivity.class);
               /* if (SearchFrom.equals("service")){
                gotocart.putExtra("PART_NO", service_partno.get(position));}
                else{*/
                gotocart.putExtra("PART_NO", service_partno.get(position));
                gotocart.putExtra("from", "vehicle");
                context.startActivity(gotocart);
            }
        });

        return convertView;
    }

    public int getCount() {
        return this.service_partno.size();
    }
}
