
package models.serviceengineer.pending;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderResultDetails {

    @SerializedName("DistrName")
    private String mDistrName;
    @SerializedName("Distributorid")
    private String mDistributorid;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("OrderedDate")
    private String mOrderedDate;
    @SerializedName("TotalOrderAmount")
    private String mTotalOrderAmount;

    public String getDistrName() {
        return mDistrName;
    }

    public void setDistrName(String DistrName) {
        mDistrName = DistrName;
    }

    public String getDistributorid() {
        return mDistributorid;
    }

    public void setDistributorid(String Distributorid) {
        mDistributorid = Distributorid;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String OrderStatus) {
        mOrderStatus = OrderStatus;
    }

    public String getOrderedDate() {
        return mOrderedDate;
    }

    public void setOrderedDate(String OrderedDate) {
        mOrderedDate = OrderedDate;
    }

    public String getTotalOrderAmount() {
        return mTotalOrderAmount;
    }

    public void setTotalOrderAmount(String TotalOrderAmount) {
        mTotalOrderAmount = TotalOrderAmount;
    }

}
