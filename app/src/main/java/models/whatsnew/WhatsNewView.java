
package models.whatsnew;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class WhatsNewView {

    @SerializedName("data")
    private List<WhatsData> mData;
    @SerializedName("result")
    private String mResult;

    public List<WhatsData> getData() {
        return mData;
    }

    public void setData(List<WhatsData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
