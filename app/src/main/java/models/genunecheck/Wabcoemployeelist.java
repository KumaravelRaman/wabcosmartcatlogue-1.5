
package models.genunecheck;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Wabcoemployeelist {

    @SerializedName("City")
    private String mCity;
    @SerializedName("Count")
    private String mCount;
    @SerializedName("UserType")
    private String mUserType;

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCount() {
        return mCount;
    }

    public void setCount(String Count) {
        mCount = Count;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String UserType) {
        mUserType = UserType;
    }

}
