package models.dealer.wishlist;

import java.util.ArrayList;

/**
 * Created by system01 on 6/13/2017.
 */

public class Wishlistdetails_model {


    private ArrayList<Integer> partidList;
    private ArrayList<Integer> WishIdList;
    private ArrayList<String> partnameList;
    private ArrayList<String> partnumberList;
    private ArrayList<String> parttypeList;
    private ArrayList<Integer> priceList;
    private ArrayList<String> descriptionList;
    private ArrayList<String> partcodeList;
    private ArrayList<String> ImageList;
    //private ArrayList<Boolean> isDeleteChecked;



    public ArrayList<Integer> getpartidList() {
        return partidList;
    }

    public void setpartidList(ArrayList<Integer> partidList) {
        this.partidList = partidList;
    }

    public ArrayList<Integer> getWishIdList() {
        return WishIdList;
    }

    public void setWishIdList(ArrayList<Integer> WishIdList) {
        this.WishIdList = WishIdList;
    }

    public ArrayList<String> getpartnameList() {
        return partnameList;
    }

    public void setpartnameList(ArrayList<String> partnameList) {
        this.partnameList = partnameList;
    }

    public ArrayList<String> getpartnumberList() {
        return partnumberList;
    }

    public void setpartnumberList(ArrayList<String> partnumberList) {
        this.partnumberList = partnumberList;
    }

    public ArrayList<String> getparttypeList() {
        return parttypeList;
    }

    public void setparttypeList(ArrayList<String> parttypeList) {
        this.parttypeList = parttypeList;
    }


    public ArrayList<Integer> getpriceList() {
        return priceList;
    }

    public void setpriceList(ArrayList<Integer> priceList) {
        this.priceList = priceList;
    }

    public ArrayList<String> getdescriptionList() {
        return descriptionList;
    }

    public void setdescriptionList(ArrayList<String> descriptionList) {
        this.descriptionList = descriptionList;
    }



    public ArrayList<String> getpartcodeList() {
        return partcodeList;
    }

    public void setpartcodeList(ArrayList<String> partcodeList) {
        this.partcodeList = partcodeList;
    }




    public ArrayList<String> getImageList() {
        return ImageList;
    }

    public void setImageList(ArrayList<String> ImageList) {
        this.ImageList = ImageList;
    }
}
