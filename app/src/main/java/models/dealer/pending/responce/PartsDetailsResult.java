
package models.dealer.pending.responce;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartsDetailsResult {

    @SerializedName("DeliveryStatus")
    private String mDeliveryStatus;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("DispatchedQty")
    private String mDispatchedQty;
    @SerializedName("PartName")
    private String mPartName;
    @SerializedName("PartNumber")
    private String mPartNumber;
    @SerializedName("PartPrice")
    private String mPartPrice;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Distributorid")
    private String mDistributorid;

    private String dealReason;
    private String cancelledBy;
    private String Dealerid;
    private boolean isCancelled = false;
    private boolean isQureySetted = false;
    private String Query;
    private String QueryDate;

    private String OrderNumber;

    public String getDealerid() {
        return Dealerid;
    }

    public void setDealerid(String dealerid) {
        Dealerid = dealerid;
    }

    public String getDeliveryStatus() {
        return mDeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus) {
        mDeliveryStatus = DeliveryStatus;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getDispatchedQty() {
        return mDispatchedQty;
    }

    public void setDispatchedQty(String DispatchedQty) {
        mDispatchedQty = DispatchedQty;
    }

    public String getPartName() {
        return mPartName;
    }

    public void setPartName(String PartName) {
        mPartName = PartName;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String PartNumber) {
        mPartNumber = PartNumber;
    }

    public String getPartPrice() {
        return mPartPrice;
    }

    public void setPartPrice(String PartPrice) {
        mPartPrice = PartPrice;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String Quantity) {
        mQuantity = Quantity;
    }


    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public String getDealReason() {
        return dealReason;
    }

    public void setDealReason(String dealReason) {
        this.dealReason = dealReason;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getDistributorid() {
        return mDistributorid;
    }

    public void setDistributorid(String Distributorid) {
        this.mDistributorid = Distributorid;
    }


    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public boolean isQureySetted() {
        return isQureySetted;
    }

    public void setQureySetted(boolean qureySetted) {
        isQureySetted = qureySetted;
    }

    public String getQuery() {
        return Query;
    }

    public void setQuery(String query) {
        Query = query;
    }

    public String getQueryDate() {
        return QueryDate;
    }

    public void setQueryDate(String queryDate) {
        QueryDate = queryDate;
    }
}
