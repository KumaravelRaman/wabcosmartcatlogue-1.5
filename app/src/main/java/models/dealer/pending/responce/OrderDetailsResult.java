
package models.dealer.pending.responce;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class OrderDetailsResult implements Serializable {

    @SerializedName("DeliveryStatus")
    private String mDeliveryStatus;
    @SerializedName("DistrName")
    private String mDistrName;
    @SerializedName("Distributorid")
    private String mDistributorid;
    @SerializedName("OrderedDate")
    private String mOrderedDate;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("TotalOrderAmount")
    private String mTotalOrderAmount;
    @SerializedName("oReason")
    private String oReason;

    private String cancelledBy;
    private String Dealerid;
    private boolean isCancelled = false;
    private boolean isQureySetted = false;
    private String Query;
    private String QueryDate;
    private String dealReason;


    private String Active;

    public String getDeliveryStatus() {
        return mDeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus) {
        mDeliveryStatus = DeliveryStatus;
    }

    public String getDistrName() {
        return mDistrName;
    }

    public void setDistrName(String DistrName) {
        mDistrName = DistrName;
    }

    public String getDistributorid() {
        return mDistributorid;
    }

    public void setDistributorid(String Distributorid) {
        mDistributorid = Distributorid;
    }

    public String getOrderedDate() {
        return mOrderedDate;
    }

    public void setOrderedDate(String OrderedDate) {
        mOrderedDate = OrderedDate;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String OrderStatus) {
        mOrderStatus = OrderStatus;
    }

    public String getTotalOrderAmount() {
        return mTotalOrderAmount;
    }

    public void setTotalOrderAmount(String TotalOrderAmount) {
        mTotalOrderAmount = TotalOrderAmount;
    }


    public String getDealReason() {
        return dealReason;
    }

    public void setDealReason(String dealReason) {
        this.dealReason = dealReason;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getDealerid() {
        return Dealerid;
    }

    public void setDealerid(String dealerid) {
        Dealerid = dealerid;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public String getQuery() {
        return Query;
    }

    public void setQuery(String query) {
        Query = query;
    }

    public String getQueryDate() {
        return QueryDate;
    }

    public void setQueryDate(String queryDate) {
        QueryDate = queryDate;
    }

    public boolean isQureySetted() {
        return isQureySetted;
    }

    public void setQureySetted(boolean qureySetted) {
        isQureySetted = qureySetted;
    }

    public String getoReason() {
        return oReason;
    }

    public void setoReason(String oReason) {
        this.oReason = oReason;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }


}
